Motion {#autoware-motion-design}
======

- @subpage motion-model
- @subpage controller-design
- @subpage reference-tracking-controller-design
- @subpage controller-reference-implementation
- RecordReplay planner
  - @subpage recordreplay-planner
  - @subpage recordreplay-planner-nodes
- @subpage trajectory-spoofer-design
- Pure pursuit controller
  - @subpage pure-pursuit
  - @subpage pure-pursuit-nodes
